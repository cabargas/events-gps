class MainController < ApplicationController

  def index
  end

  def results
    @latitude = params[:lat]
    @longitude = params[:long]

    @checkin = params[:start_date]
    @checkout = params[:end_date]
    @address = params[:address]

    start_date = params[:start_date].to_date.to_time.to_i.to_s
    end_date = params[:end_date].to_date.to_time.to_i.to_s

    event_locations = places(@latitude,@longitude)

    results = events(start_date, end_date, event_locations, @latitude, @longitude)
    @results = results.sort_by{ |r| r[:eventStarttime]}
    @rcount = @results.count
    @r_modulo = @rcount%3
    #@venues = venues_with_events(start_date, end_date, event_locations, @latitude, @longitude)
    #@venue_count = @venues.count
    #@vc_modulo = @venues.count%6

    @hash = Gmaps4rails.build_markers(@results) do |result, marker|
      marker.lat result[:eventLat]
      marker.lng result[:eventLong]
    end
  end

  def coordinates
    url = "https://maps.googleapis.com/maps/api/geocode/json?address=#{params[:address]}&key=AIzaSyDAODJ1pnNuO_YRbEjKjOpRzw8vUxfvv0Y"
    data = HTTParty.get(URI.encode(url))
    json = JSON.parse(data.body)
    puts json
    lat = json['results'][0]['geometry']['location']['lat']
    long = json['results'][0]['geometry']['location']['lng']

    redirect_to results_path(lat: lat, long: long, start_date: params[:event][:start_date], end_date: params[:event][:end_date], address: params[:address])
  end

  private

  def places lat, long
    url = "https://graph.facebook.com/v2.5/search?type=place&q=*&center=#{lat},#{long}&distance=20000&limit=2000&fields=id&access_token=1128513300514797|4PElfkqDYdVtxGSlC6aV0RzG9fc"
    data = HTTParty.get(URI.encode(url))
    json = JSON.parse(data.body)

    places = Array.new

    json["data"].each do |iter|
      places.push(iter["id"])
    end

    return places
  end

  def events checkin, checkout, places, lat, lng
    urls = Array.new

    places.each do |p|
      urls << "https://graph.facebook.com/v2.5/?ids=#{p}&fields=id,name,cover.fields(id,source),picture.type(large),location,events.fields(id,name,cover.fields(id,source),picture.type(large),description,start_time,attending_count,declined_count,maybe_count,noreply_count).since(#{checkin}).until(#{checkout})&access_token=1128513300514797|4PElfkqDYdVtxGSlC6aV0RzG9fc"
    end

    results = get_parallel(urls, urls.length)

    events = Array.new

    results.each do |resStr|
      resObj = JSON.parse(resStr)
      resObj.each do |venueId, venue|
      if venue['events'] and venue['events']['data'].length > 0
        venue['events']['data'].each do |event|
          eventResultObj = {
            #venueId: venueId,
            venueName: venue['name'],
            #venueCoverPicture: (venue['cover'] ? venue['cover']['source'] : nil),
            #venueProfilePicture: (venue['picture'] ? venue['picture']['data']['url'] : nil),
            venueLocation: (venue['location'] ? venue['location'] : nil),
            eventLat: venue['location']['latitude'],
            eventLong: venue['location']['longitude'],
            eventId: event['id'],
            eventName: event['name'],
            eventCoverPicture: (event['cover'] ? event['cover']['source'] : nil),
            eventProfilePicture: (event['picture'] ? event['picture']['data']['url'] : nil),
            eventDescription: (event['description'] ? event['description'] : nil),
            eventStarttime: (event['start_time'] ? event['start_time'] : nil),
            eventDistance: (venue['location'] ? (haversineDistance([venue['location']['latitude'], venue['location']['longitude']], [lat, lng], false)*1000).to_i : nil),
            eventTimeFromNow: calculateStarttimeDifference(checkin, event['start_time']),
            #eventStats: {
            #  attendingCount: event['attending_count'],
            #  declinedCount: event['declined_count'],
            #  maybeCount: event['maybe_count'],
            #  noreplyCount: event['noreply_count']
            #}
          }
          events << eventResultObj
        end
      end
    end
    end

    return events
  end

  def venues_with_events checkin, checkout, places, lat, lng
    urls = Array.new

    places.each do |p|
      urls << "https://graph.facebook.com/v2.5/?ids=#{p}&fields=id,name,cover.fields(id,source),picture.type(large),location,events.fields(id,name,cover.fields(id,source),picture.type(large),description,start_time,attending_count,declined_count,maybe_count,noreply_count).since(#{checkin}).until(#{checkout})&access_token=1128513300514797|4PElfkqDYdVtxGSlC6aV0RzG9fc"
    end

    results = get_parallel(urls, urls.length)

    venues_with_events = Array.new

    results.each do |resStr|
      resObj = JSON.parse(resStr)
      resObj.each do |venueId, venue|
      if venue['events'] and venue['events']['data'].length > 0
        venueResultObj = {
          venueId: venueId,
          venueName: venue['name'],
          #venueCoverPicture: (venue['cover'] ? venue['cover']['source'] : nil),
          venueProfilePicture: (venue['picture'] ? venue['picture']['data']['url'] : nil),
          eventCount: venue['events']['data'].length
        }
        venues_with_events << venueResultObj
      end
    end
    end

    return venues_with_events
  end

  def get_parallel(urls, thread_count=5, use_ssl=true)
    queue_in = Queue.new
    queue_out = Queue.new
    results = []
    urls.map { |url| queue_in << url }

    threads = thread_count.times.map do
      Thread.new do
        Net::HTTP.start('graph.facebook.com', use_ssl: use_ssl) do |http|
          while !queue_in.empty? && (url=queue_in.pop)
            uri = URI.encode(url)
            request = Net::HTTP::Get.new(uri)
            response = http.request request
            queue_out.push(response.body)
          end
        end
      end
    end
    threads.each(&:join)
    queue_out.size.times do |resp|
      results << queue_out.pop
    end
    return results
  end

  def haversineDistance(coords1, coords2, isMiles)

    def toRad(x)
      return x * Math::PI / 180
    end

    lon1 = coords1[1].to_f
    lat1 = coords1[0].to_f

    lon2 = coords2[1].to_f
    lat2 = coords2[0].to_f

    r = 6371

    x1 = lat2 - lat1
    dLat = toRad(x1)
    x2 = lon2 - lon1
    dLon = toRad(x2)
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    d = r * c

    if (isMiles)
      d /= 1.60934
    end

    return d
  end

  def calculateStarttimeDifference(currentTime, dataString)
    return Time.parse(dataString).to_i-currentTime.to_i
  end
end

